export default [
	{
		id: 'b1',
		title: 'TOP 10 TRENDS IN 2022 FOR JEWELLERY IN AUSTRALIA',
		text: `How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.`,
		img: 'https://cdn.shopify.com/s/files/1/0021/6592/articles/top-10-trends-in-2021-for-jewellery-in-australia-705063_400x.jpg?v=1642089658'
	},
	{
		id: 'b2',
		title: 'HOW TO PROPERLY CLEAN SILVER JEWELLERY',
		text: `How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.`,
		img: 'https://cdn.shopify.com/s/files/1/0021/6592/articles/how-to-properly-clean-silver-jewellery-265056_400x.jpg?v=1642089657'
	},
	{
		id: 'b3',
		title: '5 THINGS TO LOOK FOR WHEN SHOPPING FOR AUSTRALIAN JEWELLERY',
		text: `How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.`,
		img: 'https://cdn.shopify.com/s/files/1/0021/6592/articles/5-things-to-look-for-when-shopping-for-australian-jewellery-343541_600x.jpg?v=1635623487'
	},
	{
		id: 'b4',
		title: 'MOST POPULAR JEWELLERY FOR CHRISTMAS 2021',
		text: `How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.`,
		img: 'https://cdn.shopify.com/s/files/1/0021/6592/articles/most-popular-jewellery-for-christmas-2021-445986_400x.jpg?v=1638801318'
	},
	{
		id: 'b5',
		title: '10 TIPS FOR SAFELY BUYING JEWELLERY ONLINE IN AUSTRALIA',
		text: `How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.`,
		img: 'https://cdn.shopify.com/s/files/1/0021/6592/articles/australian-fashion-summer-outfit-ideas_600x.jpg?v=1637813980'
	},
	{
		id: 'b6',
		title: '5 TIPS TO FIND THE BEST AUSTRALIAN JEWELLERS',
		text: `How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.`,
		img: 'https://cdn.shopify.com/s/files/1/0021/6592/articles/we-need-to-talk-about-christmas-2021-309937_400x.jpg?v=1635493661'
	},
	{
		id: 'b7',
		title: 'WE NEED TO TALK ABOUT CHRISTMAS 2021',
		text: `How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.`,
		img: 'https://cdn.shopify.com/s/files/1/0021/6592/articles/10-tips-for-safely-buying-jewellery-online-in-australia-219838_400x.jpg?v=1638455204'
	},
	{
		id: 'b8',
		title: 'HOW TO CHOOSE THE RIGHT NECKLACE LENGTH',
		text: `How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.`,
		img: 'https://cdn.shopify.com/s/files/1/0021/6592/articles/15-jewellery-style-tips-you-need-to-know-449227_400x.jpg?v=1629837021'
	},
	{
		id: 'b9',
		title: '15 JEWELLERY STYLE TIPS YOU NEED TO KNOW',
		text: `How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.`,
		img: 'https://cdn.shopify.com/s/files/1/0021/6592/articles/10-quick-tips-to-clean-your-jewellery-821579_400x.jpg?v=1629837022'
	},
]